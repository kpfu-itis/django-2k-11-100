from django.core.management.base import BaseCommand

from notes.clickhouse import create_connection
from web.clickhouse_models import ChNote
from web.models import Note


class Command(BaseCommand):
    def handle(self, *args, **options):
        notes = Note.objects.all().select_related("user")
        clickhouse_notes = [
            ChNote(
                id=note.id,
                title=note.title,
                user_id=note.user_id,
                user_name=note.user.name,
                alert_send_at=note.alert_send_at,
                is_shared=note.is_shared,
                created_at=note.created_at,
                updated_at=note.updated_at,
            )
            for note in notes
        ]
        client = create_connection()
        client.insert(clickhouse_notes)
