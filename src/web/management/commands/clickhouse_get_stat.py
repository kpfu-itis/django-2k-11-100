from django.core.management.base import BaseCommand
from infi.clickhouse_orm import F

from notes.clickhouse import create_connection
from web.clickhouse_models import ChNote


class Command(BaseCommand):
    def handle(self, *args, **options):
        db = create_connection()
        res = ChNote.objects_in(db).aggregate(ChNote.user_id, count=F.count())
        print("user_id\tcount")
        for row in res:
            print(row.user_id, "\t", row.count)
