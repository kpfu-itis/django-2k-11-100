from infi.clickhouse_orm import (
    Model,
    DateTimeField,
    UInt64Field,
    StringField,
    NullableField,
    UInt8Field,
    MergeTree,
)


class ChNote(Model):
    id = UInt64Field()
    title = StringField()
    user_id = UInt64Field()
    user_name = NullableField(StringField())
    alert_send_at = NullableField(DateTimeField())
    is_shared = UInt8Field()
    created_at = DateTimeField()
    updated_at = DateTimeField()

    engine = MergeTree("created_at", ("id",))
