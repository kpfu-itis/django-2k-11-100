from django.core.management.base import BaseCommand

from notes.clickhouse import create_connection
from web.clickhouse_models import ChNote


class Command(BaseCommand):
    def handle(self, *args, **options):
        db = create_connection()
        db.create_table(ChNote)
        print("tables created")
